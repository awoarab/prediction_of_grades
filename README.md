# Predicting Student's Performance Using Various Machine Learning Methods



## Description

Following project was done in collaboration with other data science students for an introductory class in data science. The project concerns itself with finding the best model for predicting Portuguese students' final grade in mathematics using various academic and socioeconomic predictors. 

This repository is organised in two main directories:

1. Data directory: contains data source and a table explaining predictor variables
2. Code directory: contains a Jupyter Notebook with the final code and findings summarised. 


